$('#changeMenu').on('change', function() {
	getValue=this.value;
	if(getValue=="1"){
		$('.showParentMenu').show();
	}
	else{
		$('.showParentMenu').hide();
	}
  });


$('.coming-soon').click(function(){
	swal({
      title: "Coming Soon",
      text: "Fitur Sedang Dalam Pengembangan.",
      type: "warning"
	});
	return false;
});

function commingsoon(){
	swal({
		title: "Coming Soon",
		text: "Fitur Sedang Dalam Pengembangan.",
		type: "warning"
	  });
	  return false;
}
function confirmDelete(){
	swal({
		title: "Are you sure?",
		text: "But you will still be able to retrieve this file.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#2E59D9',
		cancelButtonColor: '#d33',
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false
		},
		function(isConfirm){
		if (isConfirm) {
			swal("Berhasil", "Data terhapus", "success");
		} else {
			swal("Cancelled", "Data tidak terhapus", "error");
		}
		});
}

function confirmDeleteHubungiKami(entity,x){
	swal({
		title: "Are you sure?",
		text: "But you will still be able to retrieve this file.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#2E59D9',
		cancelButtonColor: '#d33',
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false
		},
		function(isConfirm){
		if (isConfirm) {
			window.location.href = entity+"/"+x+"/delete";
		} else {
			swal("Cancelled", "Data tidak terhapus", "error");
		}
		});
}

function confirmDeactivate(redirectUrl){
       
	swal({
		title: "Are you sure?",
		text: "But you will still be able to retrieve this file.",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#2E59D9',
		cancelButtonColor: '#d33',
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "Cancel",
		closeOnConfirm: false,
		closeOnCancel: false
		},
		function(isConfirm){
		if (isConfirm) {
			window.location.href = redirectUrl;
		} else {
			swal("Cancelled", "Data tidak terhapus", "error");
		}
	});
}

$( document ).ready(function() {
	let current_jalur_seleksi_id = $('#current_parent_konten_id').val();
	if(current_jalur_seleksi_id!='0'){
		$('.showParentMenu').show();
	}
	else{
		$('.showParentMenu').hide();
	}
	// alert(current_jalur_seleksi_id);
});

$(".form_datetime").datetimepicker({
	format: "yyyy-mm-dd hh:ii",
	// linkField: "mirror_field",
	linkFormat: "yyyy-mm-dd hh:ii",
	showMeridian: true,
	autoclose: true,
	todayBtn: true
});
